<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package theme
 */
get_header(); ?>

<!-- CONTENT-HOME -->
<div id="content-home">
  <div class="container">
    <div class="row">

      <!-- CONTENT-ARTICLE -->
      <div class="col-md-8 col-sm-12 col-xs-12">
        <div class="content-article">
          <ul class="posts">

          <?php if ( have_posts() ) : ?>

            <?php /* Start the loop */ ?>
            <?php while ( have_posts() ) : the_post(); ?>

              <?php
              /* Include the Post-Format-specific template for the content.
               * If you want to override this in a child theme, then include a file
               * called content-___.php (where ___ is the Post Format name) and that will be used instead.
               */
                get_template_part( 'content', get_post_format() );
              ?>
            <?php endwhile; ?>

          <?php else: ?>

            <?php get_template_part('content', 'none'); ?>

          <?php endif; ?>

          </ul>
        </div><!-- .content-article -->
        <!-- Pagination -->
          <?php
            echo custom_pagination();
           ?>
        <!-- End Pagination -->
      </div><!-- .col-md-8 -->
      <!-- End CONTENT-ARTICLE -->

      <!-- Sidebar -->
      <?php get_sidebar(); ?>
      <!-- End Sidebar -->

    </div> <!-- .row -->
  </div> <!-- .container -->
</div><!-- #content-home -->
<!-- End CONTENT-HOME -->

<?php get_footer(); ?>
