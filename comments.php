<?php
/**
 * The template for displaying comments.
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package theme
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
  return;
}
?>

<div id="comments" class="comments-area">

  <?php // You can start editing here -- including this comment! ?>

  <?php if (have_comments() ) : ?>
    <h2 class="comments-title"><i class="icon icon-bubbles"></i>
      <?php
        printf( _nx('One Comment', '%1$s Comments', get_comments_number(), 'ariflaw'),
          number_format_i18n( get_comments_number() ), ' <span>' . get_the_title() . ' </span>');
       ?>
    </h2>

    <ol class="commentlist">
      <?php
          wp_list_comments( array(
            'callback' => 'ariflaw_comment',
            'style' => 'ol'
            )
          );
       ?>
    </ol> <!-- .comment-list -->

    <?php if (get_comment_pages_count() > 1 && get_option('page_comments') ) : // are there comments to navigate through ?>
      <nav id="comment-nav-above" class="comment-navigation" role="navigation">
        <h1 class="screen-reader-text"><?php _e('Comment navigation', 'Ariflaw' ); ?></h1>
        <div class="nav-previous"><?php previous_comments_link( __('&larr; Older Comments', 'Ariflaw') ); ?></div>
        <div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'Ariflaw' ) ); ?></div>
      </nav> <!-- #comment-nav-above -->
    <?php endif; // check for comment navigation ?>

    <?php
      /* If there are no comments and comments are closed, let's leave a note.
     * But we only want the note on posts and pages that had comments in the first place.
     */
      if (! comments_open() && get_comments_number() ) : ?>
      <p class="nocomments"><?php _e( 'Comment are closed', 'Ariflaw' ); ?></p>
    <?php endif; ?>

  <?php endif; // have_comments ?>

  <?php comment_form(); ?>

</div> <!-- #comment .comments-area -->
