<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Ariflaw
 */
 ?>
<!DOCTYPE HTML>
<!--[if lt IE 8 ]><html class="no-js ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 8)|!(IE)]><!--><html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>

  <meta charset="<?php bloginfo('charset'); ?>" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <?php
    //if single post then add excerpt as meta description
    if (is_single() || is_page() ) : if (have_posts() ) : while (have_posts() ) : the_post(); ?>
  <meta name="description" content="<?php echo get_the_excerpt();?>">
  <?php endwhile; endif; elseif (is_home() ): ?>
  <meta name="description" content="<?php bloginfo('description'); ?>" />
  <?php  endif; ?>
  <?php if (is_category()) {
  $cat = get_query_var('cat');
  $metacat= strip_tags(category_description($cat));?>
  <meta name="description" content="<?php echo $metacat;?>" />
  <?php } ?>
  <?php if (is_single() && in_category('1')) { ?>
  <meta name="description" content="<?php the_title();?>" />
  <?php } ?>
  <!-- Author -->
  <?php if(is_home()){ ?>
  <meta name="author" content="Arifaw" />
  <?php } else if(is_single()){?>
  <meta name="author" content="<?php echo get_the_author_meta('display_name'); ?>" />
  <?php } ?>
  <meta name="keywords" content="Worpdress Development, Tutorial WP, freebies and resource web development" />

  <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

  <!-- Favicon and Apple Icons -->
  <link rel="shortcut icon" href="images/icons/favicon.ico" />

<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<!-- WRAPPER -->
<div class="wrapper"><!-- end of wrapper on footer.php -->

  <!-- HEADER -->
  <header id="header">
    <div class="container">
      <nav id="nav" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <h1 class="site-title"><a href="<?php echo esc_url(home_url( '/' )); ?>" rel="Home"><?php bloginfo('name'); ?></a></h1>
        </div><!-- .navbar-header -->
        <!-- <div id="sb-search" class="sb-search">
          <form role="search" method="get" id="seachform" class="search-form" action="<?php echo esc_url(home_url('/')); ?>">
            <input class="sb-search-input" placeholder="Enter your search term" type="search" value="" name="s" id="search">
            <input class="sb-search-submit " type="submit" value="">
            <span class="sb-icon-search"><i class="icon icon-search"></i></span>
          </form>
        </div> --><!-- #sb-search -->
        <?php    /**
          * Displays a navigation menu
          * @param array $args Arguments
          */
          $args = array(
            'theme_location' => 'primary',
            'menu' => 'primary',
            'container' => 'div',
            'container_class' => 'collapse navbar-collapse',
            'container_id' => '',
            'menu_class' => 'nav-menu nav navbar-nav navbar-right',
            'menu_id' => '',
            'echo' => true,
            'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
            'before' => '',
            'after' => '',
            'link_before' => '',
            'link_after' => '',
            'items_wrap' => '<ul id="%1$s" class="%2$s" >%3$s</ul>',
            'depth' => 0,
            'walker' => new wp_bootstrap_navwalker()
          );

          wp_nav_menu( $args );
        ?>
      </nav>
  </header><!-- header -->
  <!-- End HEADER -->
