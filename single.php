<?php
/**
	* The template for displaying all single posts.
	*
	* @package theme
	*/

get_header(); ?>

	<!-- CONTENT -->
	<div id="content">
		<div class="container">
					<?php while ( have_posts() ) : the_post(); ?>

						<?php

							if(has_post_format('quote')){
								get_template_part( 'single-quote');
							} else if(has_post_format('audio')){
								get_template_part( 'single-audio');
							} else if(has_post_format('gallery')){
								get_template_part( 'single-gallery');
							} else if(has_post_format('video')){
								get_template_part( 'single-video');
							} else if(has_post_format('image')){
								get_template_part( 'single-image');
							} else if(has_post_format('link')){
								get_template_part( 'single-link');
							} else{
								get_template_part('single-content');
							}

						 ?>

					<?php endwhile; //end of the loop. ?>
		</div><!-- .container -->
	</div><!-- #content -->
	<!-- End CONTENT -->

<?php get_footer(); ?>
