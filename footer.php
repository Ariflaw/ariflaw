<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Ariflaw
 */
?>

<!-- FOOTER -->
<footer id="footer">
  <div class="container">
    <div class="row">
      <h1 class="site-title"><?php echo bloginfo('name'); ?></h1>
      <div class="social-list">
        <ul>
          <?php
            $fb_social = ot_get_option('facebook_url');
            if(!empty($fb_social)){ ?>
            <li><a href="<?php echo ot_get_option('facebook_url'); ?>" target="_blank"><i class="icon icon-facebook"></i></a></li>
          <?php } ?>
          <?php
            $tw_social = ot_get_option('twitter_url');
            if(!empty($tw_social)){ ?>
            <li><a href="<?php echo ot_get_option('twitter_url'); ?>" target="_blank"><i class="icon icon-twitter"></i></a></li>
          <?php } ?>
          <?php
            $gp_social = ot_get_option('googleplus_url');
            if(!empty($gp_social)){ ?>
            <li><a href="<?php echo ot_get_option('googleplus_url'); ?>" target="_blank"><i class="icon icon-googleplus"></i></a></li>
          <?php } ?>
          <?php
              $link_social = ot_get_option('linkedi_url');
            if(!empty($link_social)){ ?>
            <li><a href="<?php echo ot_get_option('linkedi_url'); ?>" target="_blank"><i class="icon icon-linkedin"></i></a></li>
          <?php } ?>
          <?php
            $pin_social = ot_get_option('pinterest_url');
            if(!empty($pin_social)){ ?>
            <li><a href="<?php echo ot_get_option('pinterest_url'); ?>" target="_blank"><i class="icon icon-pinterest"></i></a></li>
          <?php } ?>
          <?php
            $flk_social = ot_get_option('flickr_url');
            if(!empty($flk_social)){ ?>
            <li><a href="<?php echo ot_get_option('flickr_url'); ?>" target="_blank"><i class="icon icon-flickr3"></i></a></li>
          <?php } ?>
          <?php
            $drib_social = ot_get_option('dribbble_url');
            if(!empty($drib_social)){ ?>
            <li><a href="<?php echo ot_get_option('dribbble_url'); ?>" target="_blank"><i class="icon icon-dribbble"></i></a></li>
          <?php } ?>
          <?php
            $sounc_cocial = ot_get_option('soundcloud_url');
            if(!empty($sounc_cocial)){ ?>
            <li><a href="<?php echo ot_get_option('soundcloud_url'); ?>" target="_blank"><i class="icon icon-soundcloud"></i></a></li>
          <?php } ?>
          <?php
            $sky_social = ot_get_option('skype_url');
            if(!empty($sky_social)){ ?>
            <li><a href="<?php echo ot_get_option('skype_url'); ?>" target="_blank"><i class="icon icon-skype"></i></a></li>
          <?php } ?>
          <?php
            $git_social = ot_get_option('github_url');
            if(!empty($git_social)){ ?>
            <li><a href="<?php echo ot_get_option('github_url'); ?>" target="_blank"><i class="icon icon-github3"></i></a></li>
          <?php } ?>
          <?php
            $wpr_social = ot_get_option('wordpress_url');
            if(!empty($wpr_social)){ ?>
            <li><a href="<?php echo ot_get_option('wordpress_url'); ?>" target="_blank"><i class="icon icon-wordpress"></i></a></li>
          <?php } ?>
          <?php
            $vmo_social = ot_get_option('vimeo_url');
            if(!empty($vmo_social)){ ?>
            <li><a href="<?php echo ot_get_option('vimeo_url'); ?>" target="_blank"><i class="icon icon-vimeo3"></i></a></li>
          <?php } ?>
          <?php
            $ytb_social = ot_get_option('youtube_url');
            if(!empty($ytb_social)){ ?>
            <li><a href="<?php echo ot_get_option('youtube_url'); ?>" target="_blank"><i class="icon icon-youtube"></i></a></li>
          <?php } ?>
          <?php
          $stk_social = ot_get_option('stackoverflow_url');
            if(!empty($stk_social)){ ?>
            <li><a href="<?php echo ot_get_option('stackoverflow_url'); ?>" target="_blank"><i class="icon icon-stackoverflow"></i></a></li>
          <?php } ?>
          <?php
            $rss_social = ot_get_option('rss_feed');
            if(!empty($rss_social)){ ?>
            <li><a href="<?php echo ot_get_option('rss_feed'); ?>" target="_blank"><i class="icon icon-feed2"></i></a></li>
          <?php } ?>
        </ul>
      </div><!-- .social-list -->
      <div class="nav-footer">
        <?php    /**
          * Displays a navigation menu
          * @param array $args Arguments
          */
          $args = array(
            'theme_location' => 'secondary',
            'menu' => 'secondary',
            'container' => 'div',
            'container_class' => '',
            'container_id' => '',
            'menu_class' => 'menu',
            'menu_id' => '',
            'echo' => true,
            'fallback_cb' => 'wp_page_menu',
            'before' => '',
            'after' => '',
            'link_before' => '',
            'link_after' => '',
            'items_wrap' => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
            'depth' => 0,
            'walker' => ''
          );

          wp_nav_menu( $args ); ?>
      </div><!-- .nav-footer -->
      <div class="copyright">
        <p>Copyright &copy; <?php echo date('Y'); ?> <a href="<?php echo esc_url( home_url('/') ); ?>"><?php bloginfo('name'); ?></a>. All Right Reserved.</p>
        <p>Design and Develop by <a href="http://www.ariflaw.com">Arilfaw</a></p>
      </div><!-- .copyright -->
    </div><!-- .row -->
  </div><!-- .container -->
</footer>
<!-- End FOOTER -->

  <!-- Back To Top -->
    <a href="#0" class="cd-top"> <i class="icon icon-arrow-up2"></i></a>
  <!-- End Back To Top -->

</div><!-- .wrapper -->
<!-- End WRAPPER -->

<?php wp_footer(); ?>

    <script>
      $('.posts').masonry({
        itemSelector: '.item',
        columnWidth: 2
      });

    </script>

</body>
</html>
