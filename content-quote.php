<?php
/**
*
* Content Code for display content quote default Home
*
**/
 ?>

              <li class="item item-quote">
              <a href="<?php the_permalink(); ?>">
                <div id="post-<?php the_ID(); ?>" <?php post_class( 'quote-content' ); ?>>
                  <div class="entry-article-quote">
                    <?php the_content(); ?>
                    <?php
                      /**
                      * Show Meta Box Option Tree Quote
                      **/
                      $quote = get_post_meta( get_the_ID(), 'post_quote', TRUE );
                      if ( !empty($quote) ) { ?>
                        <span class="author_quote">- by <?php echo $quote ?></span>
                    <?php } ?>
                  </div>
                </div><!-- .post-class -->
              </a>
              </li><!-- .item -->
