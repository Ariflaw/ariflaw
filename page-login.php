<!DOCTYPE HTML>
<!--[if lt IE 8 ]><html class="no-js ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="no-js ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 8)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>

    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title><?php echo bloginfo('name'); ?> &rsaquo; Login</title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/login.css' ?>">

    <!-- Favicon and Apple Icons -->
    <link rel="shortcut icon" href="images/icons/favicon.ico" />
</head>
<body>

  <div class="main-login">
    <div class="login-branding">
      <a href="#" class="logo-login">Ariflaw</a>
    </div>
    <div class="login-form">
    <?php $args = array(
        'redirect' => home_url(),
        'id_username' => 'user',
        'id_password' => 'pass',
      )
    ;?>
    <?php wp_login_form($args); ?>
    </div><!-- .login-form -->
  </div><!-- .main-login -->


</body>
</html>
