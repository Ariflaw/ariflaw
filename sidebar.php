<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package theme
 */

if ( ! is_active_sidebar('sidebar')){
  return;
}

 ?>
      <!-- SIDEBAR-HOME -->
      <div class="col-md-4 col-sm-12 col-xs-12">
        <aside class="row" id="sidebar-home">
          <?php dynamic_sidebar( 'sidebar' ); ?>
        </aside>
      </div><!-- .com-md-4 -->
      <!-- End SIDEBAR-HOME -->
