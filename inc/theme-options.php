<?php
/**
 * Initialize the custom Theme Options.
 */
add_action( 'admin_init', 'custom_theme_options' );

/**
 * Build the custom settings & update OptionTree.
 *
 * @return    void
 * @since     2.0
 */
function custom_theme_options() {

	/**
	 * Get a copy of the saved settings array.
	 */
	$saved_settings = get_option( ot_settings_id(), array() );

	/**
	 * Custom settings array that will eventually be
	 * passes to the OptionTree Settings API Class.
	 */

	$custom_settings = array(
		'contextual_help' => array(
		),
		'sections'        => array(
			array(
				'id'          => 'general',
				'title'       => __( 'General', 'Ariflaw' )
      ),
			array(
				'id'          => 'layouts',
				'title'       => __( 'Layouts Settings', 'Ariflaw' )
			),
      array(
        'id'          => 'post',
        'title'       => __( 'Blog Settings', 'Ariflaw' )
      ),
			array(
				'id'          => 'social_account',
				'title'       => __( 'Social Media', 'Ariflaw' )
			),
    ),
		// General Section
		'settings' => array(
      array(
          'label' => 'Main Logo',
          'id' => 'logo',
          'type' => 'upload',
          'desc' => 'Upload the main logo for your website.',
          'std' => '',

          'section' => 'general'
      ),
      array(
          'label' => 'Favicon',
          'id' => 'fav',
          'type' => 'upload',
          'desc' => 'Upload the favicon for your website.',
          'std' => '',

          'section' => 'general'
      ),
      array(
        'label'       => __( 'Use built-in SEO fields', 'theme-text-domain' ),
        'id'          => 'seo_field',
        'type'        => 'on-off',
        'desc'        => sprintf( __( 'Turn it off if you want to use external SEO plugin.', 'theme-text-domain' ), '<code>on</code>' ),
        'std'         => 'off',

        'section' => 'general'
      ),
      array(
        'id'          => 'meta_des',
        'label'       => __( 'Meta Description', 'theme-text-domain' ),
        'desc'        => __( 'These setting may be overridden for single posts & pages.
', 'theme-text-domain' ),
        'std'         => '',
        'type'        => 'text',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'seo_field:is(on)',
        'section' => 'general'
      ),
      array(
        'id'          => 'meta_key',
        'label'       => __( 'Meta Keywoards', 'theme-text-domain' ),
        'desc'        => __( 'These setting may be overridden for single posts & pages.
', 'theme-text-domain' ),
        'std'         => '',
        'type'        => 'text',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'seo_field:is(on)',
        'section' => 'general'
      ), // End General Section
      // Post Section
      array(
          'label' => 'Main Logo',
          'id' => 'logo',
          'type' => 'upload',
          'desc' => 'Upload the main logo for your website.',
          'std' => '',

          'section' => 'post'
      ),// End Test 2 Section
      // Layout Section
      array(
          'label' => 'Main Logo',
          'id' => 'logo',
          'type' => 'upload',
          'desc' => 'Upload the main logo for your website.',
          'std' => '',

          'section' => 'layouts'
      ),
      array(
          'label' => 'Main Logo',
          'id' => 'logo',
          'type' => 'upload',
          'desc' => 'Upload the main logo for your website.',
          'std' => '',

          'section' => 'layouts'
      ),// End Test 3 Section
      // Section Footer
      array(
          'label' => 'Facebook URL',
          'id' => 'facebook_url',
          'type' => 'text',
          'desc' => 'Enter full link to your account (including http://)',
          'std' => '',

          'section' => 'social_account'
      ),
      array(
          'label' => 'Twitter URL',
          'id' => 'twitter_url',
          'type' => 'text',
          'desc' => 'Enter full link to your account (including http://)',
          'std' => '',

          'section' => 'social_account'
      ),
      array(
          'label' => 'Googleplus URL',
          'id' => 'googleplus_url',
          'type' => 'text',
          'desc' => 'Enter full link to your account (including http://)',
          'std' => '',

          'section' => 'social_account'
      ),
      array(
          'label' => 'Linkedi URL',
          'id' => 'linkedi_url',
          'type' => 'text',
          'desc' => 'Enter full link to your account (including http://)',
          'std' => '',

          'section' => 'social_account'
      ),
      array(
          'label' => 'Pinterest URL',
          'id' => 'pinterest_url',
          'type' => 'text',
          'desc' => 'Enter full link to your account (including http://)',
          'std' => '',

          'section' => 'social_account'
      ),
      array(
          'label' => 'Flickr URL',
          'id' => 'flickr_url',
          'type' => 'text',
          'desc' => 'Enter full link to your account (including http://)',
          'std' => '',

          'section' => 'social_account'
      ),
      array(
          'label' => 'Dribbble URL',
          'id' => 'dribbble_url',
          'type' => 'text',
          'desc' => 'Enter full link to your account (including http://)',
          'std' => '',

          'section' => 'social_account'
      ),
      array(
          'label' => 'Soundcloud URL',
          'id' => 'soundcloud_url',
          'type' => 'text',
          'desc' => 'Enter full link to your account (including http://)',
          'std' => '',

          'section' => 'social_account'
      ),
      array(
          'label' => 'Skype URL',
          'id' => 'skype_url',
          'type' => 'text',
          'desc' => 'Enter full link to your account (including http://)',
          'std' => '',

          'section' => 'social_account'
      ),
      array(
          'label' => 'Github URL',
          'id' => 'github_url',
          'type' => 'text',
          'desc' => 'Enter full link to your account (including http://)',
          'std' => '',

          'section' => 'social_account'
      ),
      array(
          'label' => 'Wordpress URL',
          'id' => 'wordpress_url',
          'type' => 'text',
          'desc' => 'Enter full link to your account (including http://)',
          'std' => '',

          'section' => 'social_account'
      ),
      array(
          'label' => 'Vimeo URL',
          'id' => 'vimeo_url',
          'type' => 'text',
          'desc' => 'Enter full link to your account (including http://)',
          'std' => '',

          'section' => 'social_account'
      ),
      array(
          'label' => 'Youtube URL',
          'id' => 'youtube_url',
          'type' => 'text',
          'desc' => 'Enter full link to your account (including http://)',
          'std' => '',

          'section' => 'social_account'
      ),
      array(
          'label' => 'Stackoverflow URL',
          'id' => 'stackoverflow_url',
          'type' => 'text',
          'desc' => 'Enter full link to your account (including http://)',
          'std' => '',

          'section' => 'social_account'
      ),
      array(
          'label' => 'RSS Feed URL',
          'id' => 'rss_feed',
          'type' => 'text',
          'desc' => 'Enter full link to your account (including http://)',
          'std' => '',

          'section' => 'social_account'
      ),
    )
	);



	/* allow settings to be filtered before saving */
	$custom_settings = apply_filters( ot_settings_id() . '_args', $custom_settings );

	/* settings are not the same update the DB */
	if ( $saved_settings !== $custom_settings ) {
		update_option( ot_settings_id(), $custom_settings );
	}

}
