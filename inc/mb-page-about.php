<?php
/**
 * Initialize the custom Meta Boxes.
 */
add_action( 'admin_init', 'meta_boxes_page_about' );

/**
 * Meta Boxes demo code.
 *
 * You can find all the available option types in demo-theme-options.php.
 *
 * @return    void
 * @since     2.0
 */
function meta_boxes_page_about() {

  /**
   * Create a custom meta boxes array that we pass to
   * the OptionTree Meta Box API Class.
   */
  $my_meta_box = array(
    'id'          => 'mb_post_about',
    'title'       => __( 'Meta Box About Me', 'Ariflaw' ),
    'desc'        => '',
    'pages'       => array( 'page' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
      array(
        'id'          => 'background',
        'label'       => __( 'Background', 'Ariflaw' ),
        'desc'        => __( 'Who made the quote.', 'Ariflaw' ),
        'std'         => '',
        'type'        => 'background',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
    )
  );

  /**
   * Register our meta boxes using the
   * ot_register_meta_box() function.
   */
  if ( function_exists( 'ot_register_meta_box' ) )
    ot_register_meta_box( $my_meta_box );

}
