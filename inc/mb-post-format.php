<?php
/**
 * Initialize the custom Meta Boxes.
 */
add_action( 'admin_init', 'meta_boxes_post_format' );

/**
 * Meta Boxes demo code.
 *
 * You can find all the available option types in demo-theme-options.php.
 *
 * @return    void
 * @since     2.0
 */
function meta_boxes_post_format() {

  /**
   * Create a custom meta boxes array that we pass to
   * the OptionTree Meta Box API Class.
   */
  $my_meta_box = array(
    'id'          => 'mb_post_format',
    'title'       => __( 'Post Format Option', 'Ariflaw' ),
    'desc'        => 'Will be display when you select one of post format.',
    'pages'       => array( 'post' ),
    'context'     => 'normal',
    'priority'    => 'high',
    'fields'      => array(
       array(
        'id'          => 'post_gallery',
        'label'       => __( 'Image Gallery', 'theme-text-domain' ),
        'desc'        => __( 'Create a gallery', 'theme-text-domain' ),
        'std'         => '',
        'type'        => 'gallery',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'video_select',
        'label'       => __( 'Select Option to Post Format Video', 'theme-text-domain' ),
        'desc'        => __( 'Select one of option post format video.', 'theme-text-domain' ),
        'std'         => '',
        'type'        => 'select',
        'section'     => 'option_types',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array(
          array(
            'value'       => '',
            'label'       => __( '-- Choose One --', 'theme-text-domain' ),
            'src'         => ''
          ),
          array(
            'value'       => 'url',
            'label'       => __( 'Embed video with url', 'theme-text-domain' ),
            'src'         => ''
          ),
          array(
            'value'       => 'code',
            'label'       => __( 'Embed video with code', 'theme-text-domain' ),
            'src'         => ''
          ),
        )
      ),
      array(
        'id'          => 'post_video_url',
        'label'       => __( 'Video Embed URL', 'theme-text-domain' ),
        'desc'        => __( 'Paste your video embed URL to display on homepage. ', 'theme-text-domain' ),
        'std'         => '',
        'type'        => 'text',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'video_select:is(url)',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'post_video_code',
        'label'       => __( 'Video Embed Code', 'theme-text-domain' ),
        'desc'        => __( 'Paste your video embed code to display on homepage. Recomended for width: 640px and height: 360px', 'theme-text-domain' ),
        'std'         => '',
        'type'        => 'textarea',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'video_select:is(code), video_select:not()',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'post_quote',
        'label'       => __( 'Quote by', 'theme-text-domain' ),
        'desc'        => __( 'Who made the quote.', 'theme-text-domain' ),
        'std'         => '',
        'type'        => 'text',
        'section'     => 'demo_more_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'post_link',
        'label'       => __( 'Your URL Link', 'theme-text-domain' ),
        'desc'        => __( 'Paste your URL link', 'theme-text-domain' ),
        'std'         => '',
        'type'        => 'text',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'audio_select',
        'label'       => __( 'Select Option to Post Format Audio', 'theme-text-domain' ),
        'desc'        => __( 'Select one of option post format Audio.', 'theme-text-domain' ),
        'std'         => '',
        'type'        => 'select',
        'section'     => 'option_types',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array(
          array(
            'value'       => '',
            'label'       => __( '-- Choose One --', 'theme-text-domain' ),
            'src'         => ''
          ),
          array(
            'value'       => 'url',
            'label'       => __( 'Embed audio with url', 'theme-text-domain' ),
            'src'         => ''
          ),
          array(
            'value'       => 'code',
            'label'       => __( 'Embed audio with code', 'theme-text-domain' ),
            'src'         => ''
          ),
        )
      ),
      array(
        'id'          => 'post_audio_url',
        'label'       => __( 'Audio URL Link', 'theme-text-domain' ),
        'desc'        => __( 'Paste link URL Audio. Example: https://soundcloud.com/davidguetta/david-guetta-ft-sam-martin-dangerous-robin-schulz-remix-radio-edit ', 'theme-text-domain' ),
        'std'         => '',
        'type'        => 'text',
        'section'     => '',
        'rows'        => '',
        'post_type'   => 'audio',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => 'test',
        'condition'   => 'audio_select:is(url)',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'post_audio_code',
        'label'       => __( 'Audio Link', 'theme-text-domain' ),
        'desc'        => __( 'Paste your Iframe audio, example:<br>
        < iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/106609036&amp; auto_play=false&amp; hide_related=false&amp; show_comments=true&amp; show_user=true&amp; show_reposts=false&amp; visual=true"></iframe>
          ', 'theme-text-domain' ),
        'std'         => '',
        'type'        => 'textarea',
        'section'     => '',
        'rows'        => '',
        'post_type'   => 'audio',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => 'test',
        'condition'   => 'audio_select:is(code), audio_select:not(url)',
        'operator'    => 'and'
      ),
    )
  );

  /**
   * Register our meta boxes using the
   * ot_register_meta_box() function.
   */
  if ( function_exists( 'ot_register_meta_box' ) )
    ot_register_meta_box( $my_meta_box );

}
