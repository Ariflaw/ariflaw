<?php
/**
*
* Function to display Audio embed
*
**/
function audio_post(){

  $audio_url = get_post_meta( get_the_ID(), 'post_audio_url', TRUE );
  $audio_code = get_post_meta( get_the_ID(), 'post_audio_code', TRUE );

  $audio_select = get_post_meta( get_the_ID(), 'audio_select', TRUE );
  $embed_code = wp_oembed_get($audio_url);

  if ( !empty($audio_url) && $audio_select == 'url' ){
    echo $embed_code;
  } else if( !empty($audio_code) && $audio_select == 'code' ){
    echo $audio_code;
  }

}

?>
