<?php
/**
*
* Function to display video from metabox post format video
*
**/

function video_post(){
  $video_url = get_post_meta( get_the_ID(), 'post_video_url', TRUE );
  $video_select = get_post_meta( get_the_ID(), 'video_select', TRUE );
  $video_code = get_post_meta( get_the_ID(), 'post_video_code', TRUE );
  $embed_code = wp_oembed_get($video_url);

  if ( !empty($video_url) && $video_select == 'url' ){
    echo $embed_code;
  } else if( !empty($video_code) && $video_select == 'code' ){
    echo $video_code;
  }

}

?>
