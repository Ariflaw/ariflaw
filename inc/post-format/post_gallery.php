<?php
/* --------------------------------------------------------------
    Gallery With Photoswipe
-------------------------------------------------------------- */
function gallery_post(){

  ?>

  <div id="slider" temscope itemtype="http://schema.org/ImageGallery" class="gallery ">

  <?php

    if (function_exists('ot_get_option')) {
      $images = explode( ',', get_post_meta( get_the_ID(), 'post_gallery', true ) );
      if ( !empty( $images ) ) {
        foreach ($images as $id) {
          // the_post_thumbnail('thumbnail');       // Thumbnail (default 150px x 150px max crop)
          // the_post_thumbnail('medium');          // Medium resolution (default 300px x 300px max)
          // the_post_thumbnail('large');           // Large resolution (default 640px x 640px max)
          // the_post_thumbnail('full');            // Original image resolution (unmodified)
          // the_post_thumbnail( array(100,100) );  // Other resolutions

          $full_img_src = wp_get_attachment_image_src( $id, 'full' );
          $thumb_img_src = wp_get_attachment_image_src( $id, 'medium');
          $alt = get_post_meta($id, '_wp_attachment_image_alt', true);
          $caption = get_post($id);
          $title = get_the_title($id);
          echo '
            <figure itemscope itemtype="http://schema.org/ImageObject" class="gallery-item">
              <a href="'.$full_img_src[0].'" itemprop="contentUrl" data-size="'.$full_img_src[1].'x'.$full_img_src[2].'">
                  <img src="'.$thumb_img_src[0].'" itemprop="thumbnail" alt="'.$alt.'" />
              </a>
              <figcaption itemprop="caption description" class="wp-caption-text">'.$caption->post_excerpt.'</figcaption>
            </figure>
          ';
        }
      }
    }

  ?>

 </div>

  <?php
}

/* --------------------------------------------------------------
    Gallery Home with Slick
-------------------------------------------------------------- */

function gallery_home(){
  ?>

  <div class="gallery-home">

  <?php

    if (function_exists('ot_get_option')) {
      $images = explode( ',', get_post_meta( get_the_ID(), 'post_gallery', true ) );
      if ( !empty( $images ) ) {
        foreach ($images as $id) {
          // the_post_thumbnail('thumbnail');       // Thumbnail (default 150px x 150px max crop)
          // the_post_thumbnail('medium');          // Medium resolution (default 300px x 300px max)
          // the_post_thumbnail('large');           // Large resolution (default 640px x 640px max)
          // the_post_thumbnail('full');            // Original image resolution (unmodified)
          // the_post_thumbnail( array(100,100) );  // Other resolutions

          $full_img_src = wp_get_attachment_image_src( $id, 'full' );
          $thumb_img_src = wp_get_attachment_image_src( $id, 'medium');
          $alt = get_post_meta($id, '_wp_attachment_image_alt', true);
          $caption = get_post($id);
          $title = get_the_title($id);
          echo '
              <div class="gallery-item">

                    <img src="'.$thumb_img_src[0].'" alt="'.$alt.'" />

              </div>
          ';
        }
      }
    }

  ?>

 </div>

  <?php
}

/* --------------------------------------------------------------
    Display Default Skin
-------------------------------------------------------------- */

function photoswipe(){
  ?>

<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

    <!-- Background of PhotoSwipe.
         It's a separate element, as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">

        <!-- Container that holds slides.
            PhotoSwipe keeps only 3 of them in DOM to save memory.
            Don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <button class="pswp__button pswp__button--share" title="Share"></button>

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

        </div><!-- .pswp__ui pswp__ui--hidden -->

    </div><!-- .pswp__scroll-wrap -->

</div><!-- .pswp -->

  <?php
}

/* --------------------------------------------------------------
    Include Stle and Script
-------------------------------------------------------------- */

function photoswipe_script(){
    // PhotoSwipe
    wp_enqueue_style('photoswipte-css', ARIFLAW_PARENT_URL. '/css/photoswipe/photoswipe.css');
    wp_enqueue_style('photoswipte-default', ARIFLAW_PARENT_URL. '/css/photoswipe/default-skin/default-skin.css');
    wp_enqueue_script('photoswipe-js', ARIFLAW_PARENT_URL. '/js/photoswipe/photoswipe.min.js', array('jquery'), '', true);
    wp_enqueue_script('photoswipe-default', ARIFLAW_PARENT_URL. '/js/photoswipe/photoswipe-ui-default.min.js', array('jquery'), '', true);

  }

add_action('wp_enqueue_scripts', 'photoswipe_script');

 ?>
