<?php
add_action('init', 'custom_faqs');
function custom_faqs()
{
  $labels = array(
    'name' => _x('FAQS', 'post type general name'),
    'singular_name' => _x('faq', 'post type singular name'),
    'add_new' => _x('Add New', 'FAQ'),
    'add_new_item' => __('Add New FAQ'),
    'edit_item' => __('Edit FAQ'),
    'new_item' => __('New FAQ'),
    'view_item' => __('View FAQ'),
    'search_items' => __('Search FAQs'),
    'not_found' =>  __('No FAQs found'),
    'not_found_in_trash' => __('No FAQs found in Trash'),
    'parent_item_colon' => '',
    'menu_name' => 'FAQs'
  );

  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'has_archive' => true,
    'hierarchical' => false,
    'menu_position' => 5,
    'supports' => array('title','editor','thumbnail','excerpt','custom-fields')
  );
  register_post_type('faqs',$args);
}

//add filter to ensure the text faq, or faq, is displayed when user updates a faq
add_filter('post_updated_messages', 'faq_updated_messages');
function faq_updated_messages( $messages ) {
  global $post, $post_ID;
  $messages['faqs'] = array(
    0 => '', // Unused. Messages start at index 1.
    1 => sprintf( __('FAQ updated. <a href="%s">View FAQ</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('FAQ updated.'),
    /* translators: %s: date and time of the revision */
    5 => isset($_GET['revision']) ? sprintf( __('FAQ restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('FAQ published. <a href="%s">View FAQ</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('FAQ saved.'),
    8 => sprintf( __('FAQ submitted. <a target="_blank" href="%s">Preview FAQ</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('FAQ scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview FAQ</a>'),
      // translators: Publish box date format, see http://php.net/date
      date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('FAQ draft updated. <a target="_blank" href="%s">Preview FAQ</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $messages;
}

//display contextual help for faqs
add_action( 'contextual_help', 'faq_add_help_text', 10, 3 );
function faq_add_help_text($contextual_help, $screen_id, $screen) {
  //$contextual_help .= var_dump($screen); // use this to help determine $screen->id
  if ('faqs' == $screen->id ) {
    $contextual_help =
      '<p>' . __('Things to remember when adding or editing a FAQ:') . '</p>' .
      '<ul>' .
      '<li>' . __('Specify the correct topic such as Orders. Create a new topic if necessary.') . '</li>' .
      '<li>' . __('Specify a custom excerpt - a short overview of the FAQ') . '</li>' .
      '</ul>' .
      '<p>' . __('Choose related posts using the MicroKids Related Posts Plugin below the FAQ') . '</p>' .
      '<ul>' .
      '<li>' . __('Click the Save button to save an FAQ as a draft') . '</li>' .
      '<li>' . __('Click publish to publish an FAQ. ') . '</li>' .
      '</ul></p>';
  } elseif ( 'edit-faqs' == $screen->id ) {
    $contextual_help =
      '<p>' . __('This is the list of all current FAQs in published or draft form. Hover over an FAQ and click Edit to edit, or click on the title of the FAQ to edit.') . '</p>' ;
  }
  return $contextual_help;
}
function my_rewrite_flush() {
  custom_faqs();
  flush_rewrite_rules();
}
register_activation_hook(__FILE__, 'my_rewrite_flush');
//hook into the init action and call create_FAQ_taxonomies when it fires
add_action( 'init', 'create_faq_taxonomies', 0 );
//create two taxonomies, genres and writers for the post type "FAQ"
function create_faq_taxonomies()
{
  // Add new taxonomy, make it hierarchical (like categories)
  $labels = array(
    'name' => _x( 'topics', 'taxonomy general name' ),
    'singular_name' => _x( 'topic', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Topics' ),
    'all_items' => __( 'All Topics' ),
    'parent_item' => __( 'Parent Topic' ),
    'parent_item_colon' => __( 'Parent Topic:' ),
    'edit_item' => __( 'Edit Topic' ),
    'update_item' => __( 'Update Topic' ),
    'add_new_item' => __( 'Add New Topic' ),
    'new_item_name' => __( 'New Topic Name' ),
    'menu_name' => __( 'Topics' ),
  );
  register_taxonomy('topic',array('faqs'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'topic' ),
  ));
}
?>
