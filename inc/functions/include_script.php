<?php
/* --------------------------------------------------------------
    Enqueue scripts and styles
-------------------------------------------------------------- */
function jquery_script(){
    wp_deregister_script('jquery');
    wp_register_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js', false, null );
    wp_enqueue_script('jquery');
}
if (!is_admin()) add_action('wp_enqueue_scripts', 'jquery_script', 11);


function blog_scripts(){

    // Normalizer
    wp_enqueue_style('normalizer', ARIFLAW_PARENT_URL.'/css/normalizer.min.css');

    /* bootstraps requaried bootstraps.css */
    wp_enqueue_style('bootstrap-css', ARIFLAW_PARENT_URL. '/css/bootstrap.min.css');

    wp_enqueue_style('iconmoon', ARIFLAW_PARENT_URL. '/css/icon.css');

    // Font Include
    wp_enqueue_style('open-sans-css', 'http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700');
    wp_enqueue_style('Josefin_sans', 'http://fonts.googleapis.com/css?family=Josefin+Sans:300,400,600,700');

    /* The following file needs to be created by processing styles.scss */
    wp_enqueue_style('style', get_stylesheet_uri());

    // JS Include Code Here //
    wp_enqueue_script('modernizr', ARIFLAW_PARENT_URL. '/js/modernizr.custom.js', array('jquery'), true);
    wp_enqueue_script('migrate', ARIFLAW_PARENT_URL. '/js/jquery-migrate-1.2.1.min.js', array('jquery'), '', true);
    wp_enqueue_script('classie', ARIFLAW_PARENT_URL. '/js/classie.js', array('jquery'), '', true);

    wp_enqueue_script('bootstrap-js', ARIFLAW_PARENT_URL. '/js/bootstrap.min.js', array('jquery'), '', true);

    //Startmenu JS
    wp_enqueue_script('smartmenu-js', ARIFLAW_PARENT_URL. '/js/jquery.smartmenus.min.js', array('jquery'), '', true);
    wp_enqueue_script('smartmenu-bootstrap', ARIFLAW_PARENT_URL. '/js/jquery.smartmenus.bootstrap.min.js', array('jquery'), '', true);
    // Masonry Script
    wp_enqueue_script('slick-js', ARIFLAW_PARENT_URL. '/js/masonry.pkgd.min.js', array('jquery'),'', true);
    // Slick
    wp_enqueue_script('masonry-js', ARIFLAW_PARENT_URL. '/js/slick.min.js', array('jquery'),'', true);

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
      wp_enqueue_script( 'comment-reply' );
    }
    // Custom JS
    wp_enqueue_script('scripts', ARIFLAW_PARENT_URL. '/js/scripts.js', array('jquery'), '', true);

    if (is_admin_bar_showing()) {
        wp_enqueue_style('style_admin', ARIFLAW_PARENT_URL .'/css/admin_style.css');
    }
}

add_action('wp_enqueue_scripts', 'blog_scripts');


/* --------------------------------------------------------------
    Add Script to Admin
-------------------------------------------------------------- */
function mytheme_add_init() {
        wp_enqueue_script('rm_script', ARIFLAW_PARENT_URL .'/js/script_admin.js', true, '1.0');
}
add_action( 'admin_enqueue_scripts', 'mytheme_add_init' );

?>
