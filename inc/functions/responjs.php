<?php

/* --------------------------------------------------------------
    Includ Clear Grid for IE 7
-------------------------------------------------------------- */

add_action( 'wp_head', 'responjs');

function responjs(){ ?>

  <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="<?php echo ARIFLAW_PARENT_URL ?>/js/html5shiv.min.js"></script>
  <script src="<?php echo ARIFLAW_PARENT_URL ?>/js/respond.min.js"></script>
  <![endif]-->

<?php }

 ?>
