<?php
if ( ! function_exists( 'ariflaw_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * To override this walker in a child theme without modifying the comments template
 * simply create your own ariflaw_comment(), and that function will be used instead.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @since Ariflaw Blog 1.0
 */

function ariflaw_comment( $comment, $args, $depth ){
  $GLOBALS['comment'] = $comment;
  switch ($comment->comment_type) :
    case 'pingbacks':
    case 'trackbacks':
    // Display trackback differently than normal commnet.
    ?>
    <li <?php comment_class(); ?> id="cpomment-<?php comment_ID(); ?>">
      <p><?php _e( 'Pingback:', 'Ariflaw Blog' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __('(Edit)', 'Ariflaw Blog'), '<span class="edit-link">','</span>' ); ?></p>
    </li>
    <?php
      break;
      default :
      // Proceed with normal comment.
      global $post;
    ?>
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
      <article id="comment-<?php comment_ID(); ?>" class="comment">
        <header class="comment-meta comment-author vcard">
          <?php
            echo get_avatar( $comment, 60 );
            printf( '<div class="comment-meta-content"> <cite><b class="fn">%1$s</b> %2$s</cite>',
              get_comment_author_link(),
              // If current post author is also comment author, make it known visually.
              ( $comment->user_id === $post->post_author ) ? '<span class="comment-author">' . __( 'Author', 'Ariflaw Blog') . '</span>' : ''
            );
            printf( '<a href="%1$s"><time datetime="%2$s">%3$s</time></a> </div>',
              esc_url( get_comment_link( $comment->comment_ID ) ),
              get_comment_time( 'c' ),
              /* translators: 1: date, 2: time */
              sprintf( __('%1$s at %2$s', 'Ariflaw Blog'), get_comment_date(), get_comment_time() )
            );
          ?>
        </header> <!-- .comment-meta -->

        <?php if ( '0' == $comment->comment_approved ) : ?>
          <p class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.', 'Ariflaw Blog' ); ?></p>
        <?php endif; ?>

        <section class="comment-content comment">
          <?php comment_text(); ?>
          <?php edit_comment_link( __( 'Edit', 'Arifaw Blog' ), '<p class="edit_link">', '</p>' ); ?>
        </section> <!-- .comment-content -->

        <div class="replay">
        <?php comment_reply_link( array_merge($args, array( 'replay_text' => __('Reply', 'Ariflaw Blog'), 'depth' => $depth, 'max_depth' => $args['max_depth'], 'before' => __('<i class="icon icon-forward"></i> ', 'Ariflaw') ) ) ); ?>
        </div> <!-- replay -->
      </article> <!-- #comment-## -->
  <?php
    break;
    endswitch; // end comment_type check
}
endif;

 ?>
