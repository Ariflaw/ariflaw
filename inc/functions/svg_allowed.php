<?php

/* --------------------------------------------------------------
    Allow SVG through WordPress Media Uploader
-------------------------------------------------------------- */
function cc_mime_types($mimes){
  $mimes['svg'] = "img+svg+xml";
  return $mimes;
}

add_filter('upload_mimes','cc_mime_types');



 ?>
