<?php
/* --------------------------------------------------------------
    Add Social Accuat User
-------------------------------------------------------------- */
add_action( 'show_user_profile', 'add_extra_social_links' );
add_action( 'edit_user_profile', 'add_extra_social_links' );

function add_extra_social_links( $user ){
  ?>
  <h3>User Profile Links</h3>
  <table class="form-table">
    <tr>
      <th><label for="facebook_profil">Facebook Profile</label></th>
      <td><input type="text" name="facebook_profil" value="<?php echo esc_attr( get_the_author_meta( 'facebook_profil', $user->ID ) ); ?>" class="regular-text" /></td>
    </tr>

    <tr>
      <th><label for="twitter_profil">Twitter Profile</label></th>
      <td><input type="text" name="twitter_profil" value="<?php echo esc_attr( get_the_author_meta( 'twitter_profil', $user->ID ) ); ?>" class="regular-text" /></td>
    </tr>

    <tr>
      <th><label for="google_profil">Google+ Profile</label></th>
      <td><input type="text" name="google_profil" value="<?php echo esc_attr( get_the_author_meta( 'google_profil', $user->ID ) ); ?>" class="regular-text" /></td>
    </tr>

  </table>
  <?php
}

add_action( 'personal_options_update', 'save_extra_social_links' );
add_action( 'edit_user_profile_update', 'save_extra_social_links' );

function save_extra_social_links( $user_id ){
  update_user_meta( $user_id, 'facebook_profil', sanitize_text_field( $_POST['facebook_profil']) );
  update_user_meta( $user_id, 'twitter_profil', sanitize_text_field( $_POST['twitter_profil']) );
  update_user_meta( $user_id, 'google_profil', sanitize_text_field( $_POST['google_profil']) );
}

/**
*
* Show Function HTML Author Post Info into Post
*
**/

function author_post_info(){
  $author_avatar = get_avatar( get_the_author_meta( 'ID'), 100  );
  $author_name = get_the_author_meta('display_name');
  $author_des = get_the_author_meta('description');
  $author_url = get_the_author_meta('user_url');

  $facebook_url = get_the_author_meta('facebook_profil');
  $twitter_url = get_the_author_meta('twitter_profil');
  $google_url = get_the_author_meta('google_profil');
  ?>
    <div class="author_post_info">
        <?php echo $author_avatar; ?>
      <div class="des_author">
        <span><?php printf( __('About the Author ', 'Ariflaw')); ?></span>
        <a href="<?php echo $author_url; ?>" rel="nofollow" target="_blank">
          <span class="name_author_post"><?php echo $author_name; ?></span>
        </a>
        <p class="desc_author_post">
          <?php echo $author_des; ?>
        </p>
        <div class="author_follow">
          <?php if( !empty($facebook_url)) { ?>
          <a href="<?php echo $facebook_url; ?>" target="_blank" rel="nofollow">
            <button type="button" class="author_follow_facebook"><i class="icon icon-facebook"></i><?php echo $author_name; ?></button>
          </a>
          <?php } ?>
          <?php if( !empty( $twitter_url )) { ?>
          <a href="<?php echo $twitter_url ?>" target="_blank" rel="nofollow">
            <button type="button" class="author_follow_twitter"><i class="icon icon-twitter"></i><?php echo $author_name; ?></button>
          </a>
          <?php } ?>
          <?php if (!empty($google_url)) { ?>
          <a href="<?php echo $google_url; ?>" target="_blank" rel="nofollow">
            <button type="button" class="author_follow_google"><i class="icon icon-googleplus"></i><?php echo $author_name; ?></button>
          </a>
          <?php } ?>
        </div><!-- .author_follow -->
      </div> <!-- .des_author -->
    </div> <!-- .author_post_info -->
  <?php
}

 ?>
