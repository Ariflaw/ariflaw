<?php


// Remove Title Attribut
function my_menu_notitle( $menu ){
  return $menu = preg_replace('/ title=\"(.*?)\"/', '', $menu );

}
add_filter( 'wp_nav_menu', 'my_menu_notitle' );
add_filter( 'wp_page_menu', 'my_menu_notitle' );
add_filter( 'wp_list_categories', 'my_menu_notitle' );


// codex.wordpress.org/Function_Reference/wp_tag_cloud
add_filter( 'widget_tag_cloud_args', 'widget_custom_tag_cloud' );
function widget_custom_tag_cloud($args) {

    /**
     * This way of asigning values to the $args arary
     * does not break the widgets capability to show
     * the selected taxonomy.
     */

    $args['number'] = 30;       // maximum number of tags to show
    $args['smallest'] = 15;     // font size for the least used tag
    $args['largest'] = 15;      // font size for the most used tag
    $args['unit'] = 'px';       // font sizing choice (pt, em, px, etc)
    $args['format'] = 'list';           // must be set to 'flat' for custom classes to work
    $args['separator'] = "\n";  // works better in double qoutes
    $args['orderby'] = 'name';  // name = alphabetical by name; count = by popularity
    $args['order'] = 'ASC';     // starting from A, or starting from highest count
    $args['exclude'] = '';      // ID's of tags to exclude, displays all except these
    $args['include'] = '';      // ID's of tags to include, displays none except these
    $args['taxonomy'] = 'post_tag';
    $args['link'] = 'view';             // view = links to tag view; edit = link to edit tag
    $args['echo'] = true;       // set to false to return an array, not echo it

    // Limit taxonomies to:
    // $args['taxonomy'] = array('post_tag', 'your_custom_taxonomy');

    return $args;
}

 ?>
