<?php

function post_pagination(){
 ?>
  <div class="post_navigation">
    <div class="row">
      <div class="col-md-6 col-sm-12">
        <!-- <div class="row"> -->
          <div class="alignleft_post">
            <?php next_post_link( '<span><i class="icon icon-arrow-left2"></i> %link</span>' ); ?>
          </div>
        <!-- </div> -->
      </div>
      <div class="col-md-6 col-sm-12">
        <!-- <div class="row"> -->
          <div class="alignright_post">
            <?php previous_post_link( '<span><i class="icon icon-arrow-right2"></i>%link</span>' ); ?>
          </div>
        <!-- </div> -->
      </div>
    </div>
  </div><!-- .post_navigation -->
  <?php
}

?>
