<?php
// Soundcloud has a shortcode for WordPress.com,
// but it doesn't work on self-hosted WordPress sites.
// To make the standard shortcode work, add this to your functions.php file.
// Here is what Soundcloud's "WordPress Code" shortcode looks like:
// [soundcloud url="http://api.soundcloud.com/tracks/95232600" params="" width=" 100%" height="166" iframe="true" /]

  function soundcloud( $atts, $url='' ) {
    extract(shortcode_atts(array(
      'url' => 'http://',
      'params' => 'color=ff6600&auto_play=false&show_artwork=false',
      'height' => '166',
      'width'  => '100%',
      'iframe' => 'true'
    ), $atts ) );

    $encoded_url = urlencode( $url );

    $output = '<p><iframe width="' . esc_attr( $width ) . '" height="' . esc_attr( $height ) . '" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url='. $encoded_url . '?' . $params . '"></iframe></p>';

    return $output;
  }

  add_shortcode( "soundcloud", "soundcloud" );

 ?>
