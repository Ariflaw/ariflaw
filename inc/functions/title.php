<?php
/* --------------------------------------------------------------
    Custom Filter WP_TITLE
-------------------------------------------------------------- */
/**
 * Filters wp_title to print a neat <title> tag based on what is being viewed.
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
add_theme_support( 'title-tag' );

if ( ! function_exists( '_wp_render_title_tag' ) ) :
  function theme_slug_render_title() {
?>
<title><?php wp_title( '|', true, 'right' ); ?></title>
<?php
  }
  add_action( 'wp_head', 'theme_slug_render_title' );
endif;


 ?>
