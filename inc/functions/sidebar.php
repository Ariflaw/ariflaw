<?php
/* --------------------------------------------------------------
    Sidebar
-------------------------------------------------------------- */

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function theme_widgets_init() {
     /**
    * Creates a sidebar
    * @param string|array  Builds Sidebar based off of 'name' and 'id' values.
    */
    $args = array(
      'name'          => __( 'Sidebar', 'theme_text_domain' ),
      'id'            => 'sidebar',
      'description'   => '',
      'class'         => '',
      'before_widget' => '<div id="%1$s" class="widget %2$s">',
      'after_widget'  => '</div>',
      'before_title'  => '<h3 class="widget-title">',
      'after_title'   => '</h3>'
    );

    register_sidebar( $args );

}
add_action( 'widgets_init', 'theme_widgets_init' );

 ?>
