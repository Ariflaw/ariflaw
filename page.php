<?php
/**
 * The template for displaying page.
 *
 * @package Ariflaw
 */

get_header(); ?>

  <!-- CONTENT -->
  <div id="content">
    <div class="container">
      <div class="row content-post">
        <div class="col-md-8">
          <?php while ( have_posts() ) : the_post(); ?>

             <article id="article post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php echo get_the_post_thumbnail(); ?>
                <?php the_title( '<h2 class="post-title">','</h2>' ); ?>
                <hr>
                <div class="meta-info">
                  <ul>
                      <li><i class="icon icon-calendar2"></i> <?php echo the_date(); ?></li>
                      <li><i class="icon icon-user3"></i> By <?php echo the_author_posts_link(); ?></li>
                  </ul>
                </div>
                <main class="content">
                  <?php the_content(); ?>

                  <!-- Masih binggun makenya -->
                  <?php wp_link_pages( array(
                      'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'Ariflaw' ) . '</span>',
                      'after'       => '</div>',
                      'link_before' => '<span>',
                      'link_after'  => '</span>',
                      ) );
                  ?>
                </main>
              </article>
              <?php edit_post_link('Edit this entry', '<p class="edit_link"><i class="icon icon-pencil2"></i>','</p>' ); ?>

              <?php
                // If comments are open or we have at least one comment, load up the comment template
                if ( comments_open() || '0' != get_comments_number() ) :
                    comments_template();
                endif;
              ?>

            <?php endwhile; //end of the loop. ?>
         </div><!-- .com-md-8 -->

        <!-- SIDEBAR -->
        <?php get_sidebar(); ?>
        <!-- End SIDEBAR -->

      </div><!-- .row .content-post -->
    </div><!-- .container -->
  </div><!-- #content -->
  <!-- End CONTENT -->

<?php get_footer(); ?>
