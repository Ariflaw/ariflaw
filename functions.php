<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @package Ariflaw
 */


/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
    $content_width = 717; /* pixels */
}

if ( ! function_exists( 'theme_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */

function theme_setup() {

  // Add default posts and comments RSS feed links to head.
  add_theme_support( 'automatic-feed-links' );

  /*
   * Enable support for Post Thumbnails on posts and pages.
   *
   * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
   */
  add_theme_support( 'post-thumbnails' );

  // This theme uses wp_nav_menu() in one location.
  register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'Ariflaw' ),
    'secondary' => __( 'Footer Menu', 'Ariflaw' ),
  ) );

  /*
   * Switch default core markup for search form, comment form, and comments
   * to output valid HTML5.
   */
  add_theme_support( 'html5', array(
    'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
  ) );

  // Set up the WordPress core custom background feature.
  // ***** BELUM FIX *****
  $args = array(
    'width'         => 980,
    'height'        => 60,
    'default-image' => get_template_directory_uri() . '/images/header.jpg',
    'uploads'       => true,
  );

  add_theme_support( 'custom-header', $args );

  /*
   * Enable support for Post Formats.
   * See http://codex.wordpress.org/Post_Formats
   */
  add_theme_support( 'post-formats', array(
    'aside','gallery', 'image', 'video', 'quote', 'link', 'audio'
  ) );

  // Setup the WordPress core custom background feature.
  add_theme_support( 'custom-background', apply_filters( 'theme_custom_background_args', array(
    'default-color' => '#f6f8f8',
    'default-image' => '',
  ) ) );
}
endif; // theme_setup
add_action( 'after_setup_theme', 'theme_setup' );


/* --------------------------------------------------------------
    Post Thumbnails
-------------------------------------------------------------- */
if ( function_exists('add_theme_support') ) {
  add_theme_support( 'post-thumbnails' );
  // set_post_thumbnail_size( 150, 150,  true );
}


/* --------------------------------------------------------------
    Add Editor Style ****** Belum FIX ******
-------------------------------------------------------------- */
function my_theme_add_editor_styles() {
    add_editor_style( 'custom-editor-style.css' );
}
add_action( 'after_setup_theme', 'my_theme_add_editor_styles' );


/* --------------------------------------------------------------
    Define Directory Location Constants
-------------------------------------------------------------- */
define('ARIFLAW_PARENT_DIR', get_template_directory() );

/* --------------------------------------------------------------
    Define URL Location Constant
-------------------------------------------------------------- */
define('ARIFLAW_PARENT_URL', get_template_directory_uri() );


/* --------------------------------------------------------------
    Include Functions
-------------------------------------------------------------- */
include ( ARIFLAW_PARENT_DIR .'/inc/functions/comment.php' );
include ( ARIFLAW_PARENT_DIR .'/inc/functions/excerpt.php' );
include ( ARIFLAW_PARENT_DIR .'/inc/functions/pagination.php' );
include ( ARIFLAW_PARENT_DIR .'/inc/functions/responjs.php' );
include ( ARIFLAW_PARENT_DIR .'/inc/functions/sidebar.php' );
include ( ARIFLAW_PARENT_DIR .'/inc/functions/svg_allowed.php' );
include ( ARIFLAW_PARENT_DIR .'/inc/functions/title.php' );
include ( ARIFLAW_PARENT_DIR .'/inc/functions/author_post_info.php' );
include ( ARIFLAW_PARENT_DIR .'/inc/functions/post_pagination.php' );
include ( ARIFLAW_PARENT_DIR .'/inc/functions/soundcloud.php' );
// include ( ARIFLAW_PARENT_DIR .'/inc/functions/faq.php' );
// include ( ARIFLAW_PARENT_DIR .'/inc/functions/login.php' );

// Post Format Function
include ( ARIFLAW_PARENT_DIR .'/inc/post-format/post_gallery.php' );
include ( ARIFLAW_PARENT_DIR .'/inc/post-format/post_video.php' );
include ( ARIFLAW_PARENT_DIR .'/inc/post-format/post_audio.php' );

include ( ARIFLAW_PARENT_DIR .'/inc/functions/include_script.php' );

// Front End
// include ( ARIFLAW_PARENT_DIR .'/inc/functions/social_media.php' );

/* --------------------------------------------------------------
    Themes Option Tree
-------------------------------------------------------------- */
/**
 * Required: set 'ot_theme_mode' filter to true.
 */
add_filter( 'ot_theme_mode', '__return_true' );

/**
 * Required: include OptionTree.
 */
require( trailingslashit( get_template_directory() ) . 'option-tree/ot-loader.php' );

/**
 * Theme Options
 */
require( trailingslashit( get_template_directory() ) . 'inc/theme-options.php' );

/**
 * Meta Boxes
 */
// require( trailingslashit( get_template_directory() ) . 'inc/meta-boxes.php' );
// require( trailingslashit( get_template_directory() ) . 'inc/demo-meta-boxes.php' );
require( trailingslashit( get_template_directory() ) . 'inc/mb-post-format.php' );
require( trailingslashit( get_template_directory() ) . 'inc/mb-page-about.php' );



/* --------------------------------------------------------------
    Menu
-------------------------------------------------------------- */
// Register Custom Navigation Walker
require_once('wp_bootstrap_navwalker.php');
include ( ARIFLAW_PARENT_DIR .'/inc/functions/menu_nav.php' );


/* --------------------------------------------------------------
    Social Icon
-------------------------------------------------------------- */
// 
// function social_icon($id){
//
//
//  if(!empty(ot_get_option($id))){
//    echo ot_get_option($id);
//  }
// }

?>
