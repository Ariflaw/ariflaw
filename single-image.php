<?php
/**
 * The template for displaying standard single posts.
 *
 * @package theme
 */
 ?>
        <div class="row content-post">
          <div class="col-md-8">

            <article id="article post-<?php the_ID(); ?>" <?php post_class(); ?>>
              <div class="featured-image">
                <?php if ( has_post_thumbnail() ){
                    echo the_post_thumbnail('large');
                  } ?>
                <div class="meta-info">
                  <?php the_title( '<h2 class="post-title entry-title">','</h2>' ); ?>
                  <ul>
                    <li><i class="icon icon-calendar2"></i> <?php echo the_date(); ?></li>
                    <li><i class="icon icon-user3"></i> By <?php echo the_author_posts_link(); ?></li>
                    <li><i class="icon icon-tags"></i>
                      <?php
                        // Show Category Post Link
                        $category = get_the_category();
                        if($category[0]){
                          echo '<a href="'. get_category_link($category[0]->term_id).'">' .$category[0]->cat_name.'</a>';
                        }
                      ?>
                    </li>
                  </ul>
                </div>
              </div>

              <main class="content">
                <?php the_content(); ?>

                <!-- Masih binggun makenya -->
                <?php wp_link_pages( array(
                  'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentyfourteen' ) . '</span>',
                  'after'       => '</div>',
                  'link_before' => '<span>',
                  'link_after'  => '</span>',
                  ) );
                ?>
              </main>
              <!-- Show Tags Post -->
              <?php echo get_the_tag_list('<ul class="tags_post"><h2 class="title_tags">Tags</h2><li>','</li><li>','</li></ul>'); ?>
              <!-- End Show Tags Post -->
            </article>

            <!-- Author Post Info -->
            <?php echo author_post_info(); ?>
            <!-- End Author Post Info -->

            <!-- Post pagination -->
            <?php echo post_pagination(); ?>
            <!-- End Post pagination -->

            <?php
              // If comments are open or we have at least one comment, load up the comment template
              if ( comments_open() || '0' != get_comments_number() ) :
                comments_template();
              endif;
             ?>

          </div><!-- .col-md-8 -->
          <!-- SIDEBAR -->
          <?php get_sidebar(); ?>
          <!-- End SIDEBAR -->
        </div><!-- .row .content-post -->
