<?php
/**
*
* Content Code for display content default Home
*
**/
 ?>
            <li class="item">
              <div id="post-<?php the_ID(); ?>" <?php post_class( 'main-article' ); ?>>
                <div class="image-thumbnail">
                  <?php if ( has_post_thumbnail() ){
                      echo the_post_thumbnail('large');
                    } ?>
                </div>
                <?php the_title( sprintf('<h3 class="title-post entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>

                <div class="entry-article">
                  <?php the_excerpt(); ?>
                </div>
                <div class="entry-info">
                  <div class="img-entry-info">
                    <?php echo get_avatar( get_the_author_meta( 'ID' ), 55 ); ?>
                  </div>
                  <div class="entry-time"><?php echo human_time_diff( get_the_time('U'), current_time('timestamp') ). ' ago'; ?> <span> By <?php the_author_posts_link(); ?></span>
                  </div>
                  <div class="comment_show_home">
                    <a href="<?php echo get_comments_link($post->ID); ?>">
                      <p><i class="icon icon-bubbles2"></i><?php echo comments_number( '0', '1', '%' ); ?></p>
                    </a>
                  </div>
                </div><!-- .entry-info -->
              </div><!-- .post-class -->
            </li><!-- .item -->
