(function ($){

  $(document).ready(function($) {
    /* --------------------------------------------------------------
          Toggle Meta Box based on Post Format
      -------------------------------------------------------------- */
      function toggle_metaboxes(){

        var format = $('input[name="post_format"]:checked').val();

        $('#setting_post_quote, #setting_post_gallery, #setting_post_audio, #setting_post_link, #setting_post_video, #setting_audio_select, #setting_video_select, #setting_post_audio_code').hide();

        if (format == 'gallery'){
          $('#setting_post_quote, #setting_post_audio, #setting_post_video, #setting_post_video,  #setting_audio_select, #setting_post_audio_code, #setting_video_select, #setting_post_video_code, #setting_post_video_url').fadeOut('fast');
          $('#setting_post_gallery').fadeIn('slow');
        }
        if (format == 'quote'){
          $(' #setting_post_gallery, #setting_post_audio, #setting_post_link, #setting_post_video,  #setting_audio_select, #setting_post_audio_code, #setting_video_select, #setting_video_select, #setting_post_video_code, #setting_post_video_url').fadeOut('fast');
          $('#setting_post_quote').fadeIn('slow');
        }
        if (format == 'audio'){
          $('#setting_post_quote, #setting_post_gallery, #setting_post_link, #setting_post_video, #setting_video_select, #setting_post_video_code, #setting_post_video_url').fadeOut('fast');
          $('#setting_post_audio, #setting_audio_select').fadeIn('slow');
        }
        if (format == 'link'){
          $('#setting_post_quote, #setting_post_gallery, #setting_post_audio, #setting_post_video,  #setting_audio_select, #setting_post_audio_code, #setting_video_select, #setting_post_video_code, #setting_post_video_url').fadeOut('fast');
          $('#setting_post_link').fadeIn('slow');
        }
        if (format == 'video'){
          $('#setting_post_quote, #setting_post_gallery, #setting_post_audio, #setting_post_link,  #setting_audio_select, #setting_post_audio_code').fadeOut('fast');
          $('#setting_post_video, #setting_video_select').fadeIn('slow');
        }

      }

      toggle_metaboxes();

      $('#post-formats-select input[type="radio"]').click(toggle_metaboxes);

    /* --------------------------------------------------------------
         Page About Condition
      -------------------------------------------------------------- */
      function page_about(){

        var format = $('#page_template').val();

        $('#mb_post_about').hide();

        if (format == 'page-about.php'){
          $('#mb_post_about').fadeIn('slow');
        }
      }
      page_about();

      $('#page_template').change(page_about);

  });

})(jQuery);

/* --------------------------------------------------------------
    Page About Me
-------------------------------------------------------------- */

// (function ($) {

//   var meta_box = $('#setting_background');

//   $('#setting_background').hide();

//   $('#page_template').change(function () {

//     switch($(this).val()){
//       case 'page-about.php':
//         meta_box.show();
//         break;
//       default:
//         meta_box.hide();
//         break;
//     }
//   })

// });



// (function ($){

// $(document).ready(function() {

//   function mb_page_about(){

//     var template = $('#page_template').val();

//     $('#mb_post_about').hide();

//     if ( template == 'page-about.php' ){
//       $('#mb_post_about').fadeIn('slow');
//     }
//     mb_page_about();

//     $('page_template').select(mb_page_about);
//   }
// });

// })(jQuery);
